# Maxime - README.md

## What is this?

This is my personal README to help you get to know a little bit more about me and how I work.

This README is intended to serve anyone that works directly with me at GitLab. This is not meant to be a static file, and its contents will change as I learn how to navigate through life at GitLab. I hope it can help us start having a conversation and build an awesome relationship together.

- [🦊 My GitLab Handle](https://gitlab.com/morefice)
- [⚽️ Team Page](https://about.gitlab.com/company/team/#morefice)
  <!-- * [🎯 My OKRs - WIP]() -->
  <!-- * [💻 My current tasks]() -->

## About me

My name is Maxime Orefice and I am a Backend Engineer for [Verify](https://about.gitlab.com/direction/cicd/#verify) at GitLab.

- I live in Montreal, Canada, with my wife and our cat [Pizz](https://about.gitlab.com/company/team-pets/#291-pizz). My timezone is [EST](https://time.is/EST).
- I was born and raised in [Maurepas, France](https://en.wikipedia.org/wiki/Maurepas,_Yvelines). I have been living abroad since 2016.
- Before joining GitLab, I was solving international problems at [Shopify](https://www.shopify.com/) and scaling the platform to the rest of the world.
- I believe in human beings and simplicity.
- I have always been a developer passionated about code. I studied Computer Science at [EPITA](https://www.epita.fr/) (Paris, France) and have been working with the web since 2015. I love building products with people.
- I am married to my lovely wife which I've been dating since 2012.

You can read the full [Backend Engineer job description](https://about.gitlab.com/job-families/engineering/backend-engineer/), tools I use, and what success means for Engineering in our Handbook.

### Personal principles

- Simplicity
- Communication
- Transparency

### Lifestyle

I love sports. I recently started practicing crossfit and yoga. I tried to break my work routine with at least 1 hour of exercise every day to rest my brain and feel better in my body.

I'm a big cooking fan. I like experimenting new kind of food and I take a lot pleasure to cook with my wife. Please share with me any local recommendations you would have from where you live.

## Feedback

Don't hesitate to tell me if I'm doing something wrong or if you feel offended about what I'm saying. English is my second language so forgive me if I hurt you somehow. I take feedback as gift so don't by shy, I will take it in consideration and reflect on it afterwards.

## Scheduling

My calendar is generally blocked during the morning. I usually do not accept meetings during this period because that's when I start getting in my [flow momentum](<https://en.wikipedia.org/wiki/Flow_(psychology)>). I'll make exceptions if this is urgent or if you are in a totally different timezone.

If you see a good time on my calendar inside my working hours, please schedule something (no need to ask first) on my `Google Calendar` as I don't use `Calendly`.

If you need to reschedule something, go ahead! All my calendar events are set with modify privileges and you should feel free to go ahead and move it to some other time slot that is within business hours and not conflicting with other meetings.

If you want or need to talk to me, and my schedule is not open, DM me on Slack and I will make sure we talk that day.

## How to reach me

- **GitLab:** I use my emails for my GitLab to-dos.
- **E-mail:** I read my emails multiple times a day. You should not feel obligated to read or respond to any of emails you receive from me outside of your normal hours.
- **Slack:** Direct message or mention is probrably the best way to reach me during the day. I will always have Slack open during business hours. I do not have Slack installed on my personal phone, so I will respond once I am back to the computer.
- **WhatsApp/text message:** I rather not to be contacted on WhatsApp/text message, unless it is an emergency. If you have to contact me, I am somewhat responsive, but incoming messages are not treated as priority during my day.
- **Phone call:** I rather not to be contacted by phone, unless it is an emergency. I am also disinclined to call you outside of business hours.

## Work hours

I usually default from 9:00 to 5:00 [EST](https://time.is/EST) but each week might be different and I will adjust my calendar according to it depending of the work's urgency.

## Communication style

I prefer direct communication to avoid ambiguous open questions. If I don't understand something I will not hesitate to let you know. I'm still learning how things work at GitLab so please point me the rigth direction if you feel I'm totally wrong.

When I feel I'm not making enough progress I can ask you for help. Feel free to do the same if you need to.

## Relationships

## 1:1 meetings

I love chatting with new folks at GitLab. I'm always open to coffee chat that fits in my calendar. I try to book at least 2 or 3 coffee chat a week so I can meet as many folks as possible to gain context faster and meet you all at Contribute in person.

Please don't be shy and send me an invite whenever you want.

## Footnotes

This document is new and still a WIP and I would love your feedback! Did you find the time you spent reading this valuable? Was something critical missing? Feel free to go ahead and submit an MR to this document.

Inspired by [Rayana Verissimo's](https://gitlab.com/rverissimo/readme) README.
